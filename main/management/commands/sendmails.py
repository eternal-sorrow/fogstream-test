from django.core.management.base import BaseCommand
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.conf import settings
from django.urls import reverse
from django.contrib.auth import get_user_model

from datetime import datetime
from cryptography.fernet import Fernet
from smtplib import SMTPException

from main.models import Schedule, Report

def send_scheduled_mail(email, schedule):
	"""
	Accepts email address and schedule object
	Raises SMTPException
	"""
	f = Fernet(settings.TOKEN_ENCRYPTION_KEY)
	token = f.encrypt(email.addr.encode()).decode()
	# getting hostname from ALLOWED_HOSTS is dirty but I don'd see
	# another way since we don't have a request object here
	unsubscribe_url = "http://{}{}?token={}".format(
		settings.ALLOWED_HOSTS[0],
		reverse("unsubscribe"),
		token
	)
	html_text = render_to_string("main/email.html",
		{
			"text": schedule.text,
			"url": unsubscribe_url
		}
	)
	plain_text = "{}\nUnsubscribe: {}".format(
		schedule.text,
		unsubscribe_url
	)
	# I use email of user with id=1 as from_email
	send_mail(
		subject=schedule.subject,
		message=plain_text,
		html_message=html_text,
		from_email=get_user_model().objects.get(pk=1).email,
		recipient_list=[email],
	)


class Command(BaseCommand):
	def handle(self, *args, **options):
		schedules = Schedule.objects.filter(done=False, time__lte=datetime.now())
		for schedule in schedules:
			emails = schedule.group.email_set.all()
			failed_list = []
			for email in emails:
				try:
					send_scheduled_mail(email, schedule)
				except SMTPException:
					failed_list.append(email)
			report = Report.objects.create(
				schedule=schedule,
				sent=schedule.group.email_set.count() - len(failed_list)
			)
			report.failed.set(failed_list)
			schedule.done = True
			schedule.save(update_fields=["done"])
