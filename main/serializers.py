from rest_framework import serializers

from .models import Email, Group, Schedule


class GroupSerializer(serializers.ModelSerializer):
	class Meta:
		model = Group
		fields = ('id', 'name')


class EmailSerializer(serializers.ModelSerializer):
	"""
	Displays nested group object, but only allows to write group_id instead of
	creating new group every time or not being able to write group at all.
	Possible integrity error is handled in ViewSet
	"""
	group = GroupSerializer(read_only=True)
	group_id = serializers.IntegerField(write_only=True)
	class Meta:
		model = Email
		fields = ('id', 'addr', 'group', 'group_id')


class ScheduleSerializer(serializers.ModelSerializer):
	"""
	Displays nested group object, but only allows to write group_id instead of
	creating new group every time or not being able to write group at all.
	Possible integrity error is handled in ViewSet
	"""
	group = GroupSerializer(read_only=True)
	group_id = serializers.IntegerField(write_only=True)
	class Meta:
		model = Schedule
		fields = ('id', 'subject', 'text', 'group', 'group_id', 'time', 'done')
		read_only_fields = ('done',)
