from django.urls import path, include

from rest_framework import routers

from .views import unsubscribe, EmailViewSet, GroupViewSet, ScheduleViewSet

router = routers.DefaultRouter()
router.register('email', EmailViewSet)
router.register('group', GroupViewSet)
router.register('schedule', ScheduleViewSet)

urlpatterns = [
	path("unsubscribe/", unsubscribe, name="unsubscribe"),
	path("api/", include(router.urls)),
]
