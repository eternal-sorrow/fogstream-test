from django.contrib import admin

from .models import Email, Group, Schedule, Report


admin.site.register(Group)

@admin.register(Email)
class EmailAdmin(admin.ModelAdmin):
	list_display = ('addr', 'group')


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
	list_display = ('subject', 'group', 'time', 'done')


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
	list_display = ('schedule', 'sent')
