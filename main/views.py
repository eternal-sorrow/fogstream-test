#from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.http import HttpResponseNotFound
from django.conf import settings
from django.db import IntegrityError

from rest_framework.pagination import PageNumberPagination
from rest_framework import viewsets
from rest_framework.mixins import ListModelMixin, CreateModelMixin
from rest_framework.mixins import RetrieveModelMixin, DestroyModelMixin
from rest_framework import status
from rest_framework.response import Response

from cryptography.fernet import Fernet, InvalidToken

from .models import Email, Group, Schedule
from .serializers import EmailSerializer, GroupSerializer, ScheduleSerializer

def unsubscribe(request):
	token = request.GET.get('token')
	if token is not None:
		f = Fernet(settings.TOKEN_ENCRYPTION_KEY)
		try:
			# make token valid for 5 days
			email_addr = f.decrypt(token.encode(), ttl=60*60*24*5).decode()
			email = Email.objects.get(addr=email_addr)
		except InvalidToken:
			return HttpResponseBadRequest("<h1>Invalid Token</h1>")
		except Email.DoesNotExist:
			return HttpResponseNotFound("<h1>Email does not exist</h1>")
		email.delete()
		return HttpResponse("<h1>Unsubscribed</h1>")
	return HttpResponseBadRequest("<h1>Token Required</h1>")


class PaginationClass(PageNumberPagination):
	page_size = 100
	page_size_query_param = "ps"
	max_page_size = 10000


class ApiViewSet(viewsets.GenericViewSet, ListModelMixin,  CreateModelMixin,
				 RetrieveModelMixin, DestroyModelMixin):
	lookup_url_kwarg = "id"
	pagination_class = PaginationClass


class GroupViewSet(ApiViewSet):
	queryset = Group.objects.all()
	serializer_class = GroupSerializer


class IntegrityViewSet(ApiViewSet):
	"""
	Handles IntegrityError upon object creation so that directly passed wrong
	group_id does not cause 500
	"""
	def create(self, request, *args, **kwargs):
		try:
			return super().create(request, *args, **kwargs)
		except IntegrityError:
			content = {'error': 'Group does not exist'}
			return Response(content, status=status.HTTP_400_BAD_REQUEST)


class EmailViewSet(IntegrityViewSet):
	queryset = Email.objects.all()
	serializer_class = EmailSerializer


class ScheduleViewSet(IntegrityViewSet):
	queryset = Schedule.objects.all()
	serializer_class = ScheduleSerializer
