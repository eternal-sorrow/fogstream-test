from django.db import models

# Create your models here.
class Email(models.Model):
	addr = models.EmailField(db_index=True, unique=True, verbose_name="Address")
	group = models.ForeignKey("Group", on_delete=models.CASCADE, db_index=True, verbose_name="Group")

	def __str__(self):
		return self.addr


class Group(models.Model):
	name = models.CharField(max_length=128, verbose_name="Name")

	def __str__(self):
		return self.name


class Schedule(models.Model):
	group = models.ForeignKey("Group", on_delete=models.CASCADE, verbose_name="Group")
	time = models.DateTimeField(db_index=True, verbose_name="Scheduled time")
	done = models.BooleanField(default=False, db_index=True, verbose_name="Done")
	subject = models.CharField(max_length=256, verbose_name="Subject")
	text = models.TextField(verbose_name="text")

	def __str__(self):
		return '"{}" to {} at {}'.format(self.subject, self.group.name, self.time.strftime("%c"))


class Report(models.Model):
	schedule = models.ForeignKey("Schedule", on_delete=models.SET_NULL, null=True, verbose_name="Schedule")
	sent = models.IntegerField(verbose_name="Sent")
	failed = models.ManyToManyField("Email", verbose_name="Failed email list")
